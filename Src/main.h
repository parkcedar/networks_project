/**
 @file
 @brief Includes all common header files for the project. As well as some basic utility macros

 main.h
 Networks-Project

 Created on 30/4/19.
 
 Copyright © 2019 All rights reserved.
 @author Lachlan Russell        22414249
 @author Benjamin Longbottom    22234771
 */

#ifndef main_h
#define main_h

#define DEPRECATED __attribute__((deprecated))

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include <netinet/in.h>
#include <poll.h>

// ===--- POSIX prototypes for use on linux ---=== //

// Prototypes to get around POSIX standard stuff
#ifdef __linux__
#include <getopt.h>
int kill(int pid, int sig);
int getopt(int argc, char* const argv[], const char* optstring);
int usleep(__useconds_t usec);
#endif

// ===--- TYPE DEFINITIONS and standard macros ---=== //

#define MAX_EVER_PLAYERS 50
#define TIMEOUT 30000 // In miliseconds.
#define MIN_PLAYERS 4
#define MILI 1000
#define MESSAGE_SIZE 15
#define BUFFER_SIZE 1024

/**
 Types of messages sent from a client to the server.

 @const INVALID Default message type.
 @const INIT    Initialises the connection.
 @const MOV     Sends a move to the server.
 */
enum clientMessageType {
    INVALID,
    INIT,
    MOV
};

/**
 Types of game moves sent from the client to the server.

 @const EVEN
 @const ODD
 @const DOUB
 @const CON
 */
enum moveType {
    EVEN,
    ODD,
    DOUB,
    CON,
};

/**
 A container to store the client message once it has been parsed.

 @var clientID    Stores the client id.
 @var messageType The type of message.
 @var move        The move type.
 @var contains    The contains value, specifically for the CON move. -1 if not used.
 */
typedef struct _clientMessage {
    int clientId;
    enum clientMessageType messageType;
    enum moveType move;
    int contains;
} CLIENT_MESSAGE;

/**
 Structure that stores the information about an individual player.

 @var   id          3 digit id that identifies a player
 @var   previous    Player previous in the linked list. null if no player.
 @var   next        Player next in the linked list. null if no player.
 @var   lives       Number of lives left
 @var   passedLastRound     True if their move was correct, fase otherwise.
 @var   alive       Set to false if the player has run out of lives. True otherwise.

 @var   fd          The index to the sockets file descriptor stored in GAME.
 @var   read        Set to true if the client has a message ready to read from the socket.

 @var   clientInfo  Holds client information for socket.
 @var   lastMessage Holds the last message sent from the player (After parsing).
 */
typedef struct _player {
    int id;
    struct _player* previous;
    struct _player* next;
    int lives;
    bool passedLastRound;
    bool alive;
    int fd;
    bool read;
    struct sockaddr_in* clientInfo;
    CLIENT_MESSAGE* lastMessage;
} PLAYER;

/**
 Stored information regarding the game state.

 @var   playerCount Remaining players in the game.
 @var   maxPlayers  User specified maximum players in the game.
 @var   serverFd    The socket for the server.

 @var   readSet     Contains file descriptors for all players in the game. Used to poll for activity.
 @var   serverInfo  Holds server socket information.

 @var   playerHead  Front of the linked list of players.
 @var   playerTail  End of the linked list of players.
 */
typedef struct _game {

    int playerCount;
    int maxPlayers;
    int serverFd;
    struct pollfd readSet[MAX_EVER_PLAYERS];
    struct sockaddr_in* serverInfo;
    // Head of a linked list containing players
    PLAYER* playerHead;
    PLAYER* playerTail;
} GAME;

// ===--- UTILITY MACROS ---=== //

/**
 @brief         Tests if a pointer is equal to NULL for use after an attempted memory allocation.
 
 @param _ptr    The pointer to test.
 @return        No success indicator.
 @warning       Exits on failure.
 */
#define CATCH(_ptr, _str)   \
    if (_ptr == NULL) {     \
        perror(_str);       \
        exit(EXIT_FAILURE); \
    }

// Some standard error messages for use with catch
#define eMEMORY "Memory allocation failure."
#define eSTRING "Iterating past end of string."
#define eSOCKET "Socket I/O failure."
#define ePLAYER "Iterating past end of players."

#endif /* main_h */
