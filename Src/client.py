#! /usr/bin/env python3

## @brief   A basic client to connect to our server and play the game with randomly selected moves.
#  Adapted from code distributed by the University of Western Australia. (Authors unknown).
#
#  client.py
#  Networks-Project
#
#
#  Copyright © 2019 All rights reserved.
#  @author Lachlan Russell        22414249
#  @author Benjamin Longbottom    22234771

import socket
from time import sleep
import random


# GLOBALS
class Game:
    def __init__(self, player_id, num_players, my_lives):
        self.player_id = player_id
        self.num_players = num_players
        self.my_lives = my_lives
        self.round = 0


# Parses the message type from a message sent from the server and calls the respective actions.
# @param message    The message sent from the server.
# @return           0 on success and 1 on failure.
def parse_message(new_message):
    message_type = [("WELCOME", init), ("START", start), ("PASS", pass_func), ("FAIL", fail), ("ELIM", elim),
                   ("VICT", vict), ("REJECT", gracefulExit), ("CANCEL", gracefulExit)]

    print(new_message)
    for element in message_type:
        if element[0] in new_message:
            element[1](new_message)
            return 0
    return 1


## Initiate the player from the welcome message.
def init(new_message):
    thisGame.player_id = int(new_message.split(',')[1])


## The game is starting. Initiate the game and start the round.
def start(new_message):
    thisGame.num_players = int(new_message.split(',')[1])
    thisGame.my_lives = int(new_message.split(',')[2])
    next_round()


## Passed the last round. Move onto the next round.
def pass_func(new_message):
    print("pass!")
    next_round()


## The last round was lost.
# moves the game onto the next round, lives decreaces by 1.
def fail(new_message):
    print("fail!")
    thisGame.my_lives -= 1
    next_round()


## Eliminated. Prints the message then exits gracefully.
def elim(new_message):
    print("elim!")
    exit()


## Game won. Prints the success then exits gracefully.
def vict(new_message):
    print("vict!")
    gracefulExit(new_message)


## Exits the game gracefully by closing the socket first.
def gracefulExit(new_message):
    print("Exiting gracefully:", new_message)
    sock.close()
    exit()


## Sends a move to the server over the socket.
def sendMove(message):
    message = message.encode()
    print('sending: %s' % message)
    sock.sendall(message)


## Builds a CONtains move.
# Randomly selects a digit betweeen 1-6 to form the move type of the MOV packet.
# @return A string containing the move type.
def con_move():
    move = "CON,"
    digit = random.randint(1, 6)
    move += str(digit)
    return move


## Progresses the game to the next round.
# A new move is selected randomly by choosing a move from an array. If con_move is chosen a separate function is called
# do build the extra data required for this packed.
# the message is then sent to the server.
def next_round():
    print("\nRound:" , thisGame.round, "\tLives:", thisGame.my_lives);
    possible_moves = ["EVEN", "ODD", "DOUB", con_move()]
    message = str(thisGame.player_id) + ",MOV," + possible_moves[random.randint(0, 3)]
    sendMove(message)
    thisGame.round += 1


## The main function
#
# Initiates a connection with a server (hard-coded to localhost on port 4444)
# Once the connection is established the client will begin a forever-loop reading messages from the server and
# calling parse_message() which will respond based on a map of messages to functions.
# If an exception occurs, or an invalid message is processed, the program will close the socket and exit.
if __name__ == '__main__':

    thisGame = Game(-1, -1, -1)

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    server_address = ('localhost', 4444)
    print('connecting to %s port %s' % server_address)
    sock.connect(server_address)

    try:
        # Attempt a connection
        message = 'INIT'.encode() 
        print('sending: %s' % message)
        sock.sendall(message)

        while True:
            recieved = sock.recv(15).decode()
            assert parse_message(recieved) == 0, ("Recieved an invalid message. " + str(recieved))
    finally:
        sock.close()
