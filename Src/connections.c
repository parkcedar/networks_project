/**
 @file
 @brief Handles network connections and related messaging with the clients.

 connections.c
 Networks-Project

 Created on 30/4/19.
 Copyright © 2019 All rights reserved.
 @author Lachlan Russell        22414249
 @author Benjamin Longbottom    22234771
 */

#include "connections.h"
#include "game.h"

/**
 Reads a message from a clients socket.

 @warning           Returns a null pointer if the read was unsuccessful.
 @param client_fd   The clients file descriptor to the socket.
 @return            The message from the client.
 */
char* readMessage(int client_fd)
{
    char* message = calloc(BUFFER_SIZE, sizeof(char));
    CATCH(message, eMEMORY);

    if (recv(client_fd, message, BUFFER_SIZE, 0) < 0) {
        perror("Client read failed\n");
        free(message);
        return NULL;
    }
    return message;
}

/**
 Sends a message to a client.

 @param client_fd   The client descriptor to send the message to.
 @param message     The message to send to the client.
 @return            0 on success. -1 on failure.
 */
int sendMessage(int client_fd, char* message)
{
    if (send(client_fd, message, strlen(message), 0) < 0) {
        perror("Client write failed\n");
        return -1;
    }
    return 0;
}

/**
 Sends the same message to all players.
 @note  Handles disconnecting players since it wraps the sendMessage function.

 @param game    A struct holding the game information.
 @param msg     The message to send to all players.
 @return        0 on success. -1 on failure.
 */
void sendMsgToAllPlayers(GAME* game, char* msg)
{
    for (PLAYER* p = game->playerHead; p != NULL; p = p->next) {
        if (sendMessage(game->readSet[p->fd].fd, msg) == -1) {
            // Player disconnected.
            playerDisconnected(game, p);
        }
    }
}

/**
 Parses a game message (from the client to the server) into a given CLIENT_MESSAGE structure.

 @param container   A reference to a CLIENT_MESSAGE structure.
 @param message     The message buffer to parse.

 @return    -1 if player determined to be cheating. 0 otherwise.
 */
int parseMessage(CLIENT_MESSAGE* container, char* message)
{
    const char sep[] = ",";
    char* buffer = calloc(MESSAGE_SIZE, sizeof(char));

    printf("PARSE MESSAGE : %i\n\tmessage: %s\n", container->clientId, message);

    // Initiate a strtok sequence
    if ((buffer = strtok(message, sep)) == NULL) {
        // This is a recurring pattern. If we recieve an unexpected end of string the message
        // will be returned as invalid.
        fprintf(stderr, "Message was NULL\n");
        container->messageType = INVALID;
        return 0;
    }

    // ***** NOTE ***** // strcmp returns 0 for success
    if (strcmp(buffer, "INIT") == 0) {
        container->messageType = INIT;
        return 0;
    }

    // Detect players cheating (Have sent a message with the incorrect clientid)
    if (atoi(buffer) != container->clientId) {
        container->messageType = INVALID;
        // Player cheated.
        printf("PLAYER CHEATED");
        return -1;
    }

    if ((buffer = strtok(NULL, sep)) == NULL) {
        container->messageType = INVALID;
        return 0;
    }

    // Detect messages which are not MOV and thus invalid.
    if (strcmp(buffer, "MOV") != 0) {
        container->messageType = INVALID;
        return 0;
    }
    container->messageType = MOV;

    char* types[] = { "EVEN", "ODD", "DOUB", "CON" };
    buffer = strtok(NULL, sep);

    // Iterate over the types array. If the buffer is the same as a type in the array, we cast the integer i (since
    // this is the same value as the enum.
    // Note the value '4' is highly depended on above array and enum.
    bool validMove = false;
    for (int i = 0; i < 4; i++) {
        if (strcmp(buffer, types[i]) == 0) {
            container->move = (enum moveType)i;
            validMove = true;
        }
    }
    if (!validMove) {
        container->messageType = INVALID;
        return 0;
    }

    container->contains = -1; // set to -1 in case it is not use.

    if ((buffer = strtok(NULL, sep)) != NULL) {
        if (container->move != CON) {
            container->messageType = INVALID;
            return 0;
        }
        // check move is valid, i.e. atoi returned [1-6]. Kick cheating player if not valid
        container->contains = atoi(buffer);
        if (container->contains < 1 || container->contains > 6) {
            return -1;
        }
    }
    return 0;
}

/**
 @brief     Accepts a connection from a client and constructs a player.

 Attempt to connect and initialise a new player. Once the player is accepted the message is paresed to confirm
 the INIT message. If any other message type the player is REJECTed and then torn down.

 Once the connection is successful and the message was correct, constructPlayer() is called to initialise the
 player with default values.

 @warning   This function will block until a connection is ready to accept. It is recommended to call
            poll first to guarantee a connection is ready and waiting.

 @param lives   The number of lives to initiate a player to.
 @return        True: player successfully created, False: Error while creating player.
 */
bool acceptConnection(GAME* game, int lives)
{
    // Make an empty player struct. Calloc is used to memset the default values.
    PLAYER* player = calloc(1, sizeof(PLAYER));
    CLIENT_MESSAGE* recieved = calloc(1, sizeof(CLIENT_MESSAGE));
    CATCH(player, eMEMORY);
    CATCH(recieved, eMEMORY);
    player->lastMessage = recieved;

    // clientSize needs to be initialised for the accept system call below.
    socklen_t clientSize = sizeof(*(player->clientInfo));
    player->clientInfo = calloc(1, clientSize);
    CATCH(player->clientInfo, eMEMORY);

    // Blocks program until a new player is detected.
    player->fd = accept(game->serverFd, (struct sockaddr*)player->clientInfo, &clientSize);

    // Checks if player connection was successful.
    if (player->fd < 0) {
        perror("Setup Game: Could not establish new player connection");
        exit(EXIT_FAILURE);
    }

    char* read = readMessage(player->fd);
    if (read == NULL) {
        playerDisconnected(game, player);
        return false;
    }

    parseMessage(recieved, read);

    if (recieved->messageType != INIT) {
        printf("Rejcting Client\n");
        sendMessage(player->fd, "REJECT");
        free(player->clientInfo);
        free(player->lastMessage);
        free(player);
        return false;
    }
    return constructPlayer(game, player, lives);
}

/**
 Rejects an incoming client. A full player is not formed just the bare bones to accept the connections, then send a
 REJECT packet.

 @param serverFd    File descriptor for the servers socket.
 */
void rejectConnection(int serverFd)
{
    socklen_t clientSize = sizeof(struct sockaddr_in);
    struct sockaddr_in* clientInfo = calloc(1, clientSize);
    CATCH(clientInfo, eMEMORY);

    int clientFd = accept(serverFd, (struct sockaddr*)clientInfo, &clientSize);

    // Checks if player connection was successful.
    if (clientFd < 0) {
        perror("Could not establish new player connection");
        exit(EXIT_FAILURE);
    }

    printf("Rejcting incomming Client\n");
    // Failure to send message is ignored as player info is not kept anyway.
    sendMessage(clientFd, "REJECT");
    close(clientFd);
    free(clientInfo);
}

/**
 Waits for moves from the players and attaches the moves to their PLAYER struct. A timeout limits the hang time.

 @note  File descriptor alerts are set to off for all players who have sent a message. This will be turned back on in
        the call to readMoves.
 */
void waitForMoves(GAME* game)
{
    int timeout = TIMEOUT;
    int moves = 0;
    int startTime = time(NULL); // Convert to miliseconds
    struct pollfd* socketfds = game->readSet;

    while (timeout > 0 && moves < game->playerCount) {
        // Program blocks here:
        int rc = poll(socketfds, MAX_EVER_PLAYERS, timeout);
        if (rc == 0) {
            fprintf(stderr, "Poll timed out.\n");
            break;
        }
        if (rc == -1) {
            perror("Poll failed");
            break;
        }
        if (socketfds[0].revents == POLLIN) {
            // REJECTs incomming connections mid game.
            rejectConnection(game->serverFd);
            socketfds[0].revents = 0;
        }
        for (PLAYER* player = game->playerHead; player != NULL; player = player->next) {
            short result = socketfds[player->fd].revents;
            if (result == POLLIN) {
                player->read = true;
                socketfds[player->fd].events = 0; // Turn off events.
                moves++;
            } else if (result != 0) {
                // Unexpected event
                socketfds[player->fd].events = 0; // Turn off events.
                moves++;
            }
        }
        timeout = TIMEOUT - ((time(NULL) - startTime) * 1000);
    }
    readMoves(game);
}

/**
 Prints out the final game results and tears down the game. Including canceling of all connections to remaining players.
 @note The program exits (with status SUCCESS) after completeing this function.
 */
void teardownGame(GAME* game)
{
    for (int i = 0; i < MAX_EVER_PLAYERS; i++) {
        close(game->readSet[i].fd);
    }
    exit(EXIT_SUCCESS);
}
