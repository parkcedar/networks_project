/**
 @file
 @brief Handles functions relating to the game logic

 game.h
 Networks-Project

 Created on 30/4/19.
 
 Copyright © 2019 All rights reserved.
 @author Lachlan Russell        22414249
 @author Benjamin Longbottom    22234771
 */

#ifndef game_h
#define game_h

#ifndef main_h
#include "main.h"
#endif

void notifyPlayers(GAME* game);
void readMoves(GAME* game);

void playerDisconnected(GAME* game, PLAYER* player);
void removePlayer(GAME* game, PLAYER* player);
bool constructPlayer(GAME* game, PLAYER* player, int lives);

void playGameRound(GAME* game);
void setupGame(GAME* game, int lives);

#endif /* game_h */
