/**
 @file
 @brief Handles network connections and related messaging with the clients.

 connections.h
 Networks-Project

 Created on 30/4/19.
 
 Copyright © 2019 All rights reserved.
 @author Lachlan Russell        22414249
 @author Benjamin Longbottom    22234771
 */
#ifndef connections_h
#define connections_h

#ifndef main_h
#include "main.h"
#endif

char* readMessage(int client_fd);
int sendMessage(int client_fd, char* message);

void sendMsgToAllPlayers(GAME* game, char* msg);
int parseMessage(CLIENT_MESSAGE* container, char* message);

bool acceptConnection(GAME* game, int lives);
void rejectConnection(int serverFd);

void waitForMoves(GAME* game);
void teardownGame(GAME* game);

#endif /* connections_h */
