/**   
 @file
 @brief Handles functions relating to the game logic

 game.c
 Networks-Project

 Created on 30/4/19.
 
 Copyright © 2019 All rights reserved.
 @author Lachlan Russell        22414249
 @author Benjamin Longbottom    22234771
 */

#include "game.h"
#include "connections.h"

/**
 Initialises a player with default values then sends a WELCOME packet to the client.
 @note      The player is added to the end of the linked list of players stored in game.
 @return    True: player successfully connected, False: Connection to player was lost.
 */
bool constructPlayer(GAME* game, PLAYER* player, int lives)
{

    // Initialise the file desciptors.
    struct pollfd* socketfds = game->readSet;
    socketfds[game->playerCount + 1].fd = player->fd;
    socketfds[game->playerCount + 1].events = POLLIN;
    player->fd = game->playerCount + 1; // player.fd is switched to the index of the fileDescriptors array.

    player->id = game->playerCount + 100; // 100 is the starting id, increments for each player.
    player->lives = lives;
    player->alive = true;

    // Add the player to the end of the linked list.
    player->next = NULL;
    if (game->playerTail != NULL) {
        player->previous = game->playerTail;
        game->playerTail->next = player;
    }
    game->playerTail = player;
    if (game->playerHead == NULL) {
        game->playerHead = player;
    }
    game->playerCount++;

    char message[MESSAGE_SIZE];
    snprintf(message, MESSAGE_SIZE * sizeof(char), "WELCOME,%d", player->id);
    printf("%s\n", message);
    if (sendMessage(socketfds[player->fd].fd, message) == -1) {
        playerDisconnected(game, player);
        return false;
    }
    return true;
}

/**
 Remove a player a player from the linked list implemented by game.
 @warning       The player must have been allocated before it can be freed.
 @warning       Does not decrement playerCounter in game.

 @param game    The game to remove the player from.
 @param player  A pointer to the player to remove and free.
 */
void removePlayer(GAME* game, PLAYER* player)
{
    printf("Removing player, id: %d, ", player->id);
    if (player->previous == NULL) {
        if (player->next == NULL) {
            game->playerTail = NULL;
        } else {
            player->next->previous = NULL;
        }
        game->playerHead = player->next;
    } else {
        if (player->next == NULL) {
            game->playerTail = player->previous;
        } else {
            player->next->previous = player->previous;
        }
        player->previous->next = player->next;
    }

    // Clear file descriptors.
    close(game->readSet[player->fd].fd);
    game->readSet[player->fd].events = 0;
    game->readSet[player->fd].revents = 0;

    // Free the players memory.
    if (player->clientInfo != NULL)
        free(player->clientInfo);
    if (player->lastMessage != NULL)
        free(player->lastMessage);
    free(player);
    printf("Player removed\n");
}

/**
 Wrapper for remove player to assure player count is decremented when a player disconnects.

 @param player  The player who disconnected.
 */
void playerDisconnected(GAME* game, PLAYER* player)
{
    game->playerCount--;
    removePlayer(game, player);
}

/**
 Removes players whose attribute 'alive' is set to false.
 This is achieved by iterating over every player in the linked list.

 Players are updated with an ELIM or VICT packet if they are removed from the game.
 @param winners     Set to true if everyone is a winner (i.e. all players are eliminated at the same time).
 */
void removePlayers(GAME* game, bool winners)
{
    char* msg = calloc(MESSAGE_SIZE, sizeof(char));
    CATCH(msg, eMEMORY);
    PLAYER* pNext;
    for (PLAYER* p = game->playerHead; p != NULL;) {
        pNext = p->next;
        if (!p->alive) {
            if (winners) {
                snprintf(msg, MESSAGE_SIZE * sizeof(char), "%d,VICT", p->id);
            } else {
                snprintf(msg, MESSAGE_SIZE * sizeof(char), "%d,ELIM", p->id);
            }
            sendMessage(game->readSet[p->fd].fd, msg);
            removePlayer(game, p);
            memset(msg, 0, MESSAGE_SIZE * sizeof(char));
        }
        p = pNext;
    }
    free(msg);
}

/**
 Reads moves from each players file descriptor. Updates the players lastMessage field in place after parsing the
 message.

 @note  File descriptor alerts are set to POLLIN.
 */
void readMoves(GAME* game)
{

    struct pollfd* socketfds = game->readSet;

    PLAYER* pNext;
    for (PLAYER* player = game->playerHead; player != NULL; player = pNext) {

        pNext = player->next; // If player disconnects this maps to their old next field.
        // Load struct with default values.
        CLIENT_MESSAGE* recieved = player->lastMessage;
        recieved->clientId = player->id;
        recieved->messageType = INVALID;
        recieved->contains = -1;

        // Turn events back on for all players
        if (socketfds[player->fd].events == 0) {
            socketfds[player->fd].events = POLLIN;
        }

        if (!player->read) {
            printf("%i Player has no message\n", player->id);
            continue; /* No message recieved message stays INVALID*/
        }

        char* message = readMessage(socketfds[player->fd].fd);
        if (message == NULL) {
            playerDisconnected(game, player);
            continue;
        }
        if (parseMessage(recieved, message) == -1) {
            playerDisconnected(game, player);
        }
        free(message);
    }
}

/**
 Implements the functionality for a round of the game. 'Roll's the dice. Checks if the move by the client is correct.
 Updates game state depending on their success or failure.

 @note This does not notify the players of their success, dice roll, or any other game state information.
 */
void playGameRound(GAME* game)
{
    // role dice
    int dice1 = (rand() % 6) + 1;
    int dice2 = (rand() % 6) + 1;

    printf("\ndice: %d, %d: %d\n", dice1, dice2, dice1 + dice2);

    for (PLAYER* p = game->playerHead; p != NULL; p = p->next) {
        // Remove lifes if message was wrong or invalid.
        CLIENT_MESSAGE* pMsg = p->lastMessage;
        fprintf(stderr, "Player %d, ", p->id);

        if (pMsg->messageType != MOV) {
            fprintf(stderr, "%d:sent:%d, an invalid move: ", p->id, pMsg->messageType);
            p->lives--;
            p->passedLastRound = false;
        } else {
            switch (pMsg->move) {
            case EVEN:
                fprintf(stderr, "even ");
                if ((dice1 + dice2) % 2 != 0 || dice1 == dice2) {
                    p->lives--;
                    p->passedLastRound = false;
                } else {
                    p->passedLastRound = true;
                }
                break;
            case ODD:
                fprintf(stderr, "odd ");
                if ((dice1 + dice2) % 2 != 1 || (dice1 + dice2) <= 5) {
                    p->lives--;
                    p->passedLastRound = false;
                } else {
                    p->passedLastRound = true;
                }
                break;
            case DOUB:
                fprintf(stderr, "doub ");
                if (dice1 != dice2) {
                    p->lives--;
                    p->passedLastRound = false;
                } else {
                    p->passedLastRound = true;
                }
                break;
            case CON:
                fprintf(stderr, "con ");
                if (dice1 != pMsg->contains && dice2 != pMsg->contains) {
                    p->lives--;
                    p->passedLastRound = false;
                } else {
                    p->passedLastRound = true;
                }
                break;
            }
        }
        fprintf(stderr, "\n");
    }
}

/**
 @brief Iterate over each player and notify them of the game standing.

 This is responsible for sending PASS, FAIL messages.
 Decrments the player count and sets the boolean value of alive on players to false if they loose all their lives.

 @note  Calls removePlayers to send ELIM or VICT packets to players who are no longer "alive" and removes them from
        the game.
 */
void notifyPlayers(GAME* game)
{

    printf("\nResults:\n");

    char message[MESSAGE_SIZE];
    for (PLAYER* p = game->playerHead; p != NULL; p = p->next) {
        if (p->passedLastRound) {
            printf("%d: PASS ", p->id);
            snprintf(message, MESSAGE_SIZE * sizeof(char), "%d,PASS", p->id);
            sendMessage(game->readSet[p->fd].fd, message);
        } else if (p->lives > 0) {
            printf("%d: FAIL lives left: %d", p->id, p->lives);
            snprintf(message, MESSAGE_SIZE * sizeof(char), "%d,FAIL", p->id);
            sendMessage(game->readSet[p->fd].fd, message);
        } else {
            printf("%d: FAIL lives left: %d", p->id, p->lives);
            // flag player for removal
            p->alive = false;
            game->playerCount--;
        }
        printf("\n");
    }

    if (game->playerCount < 1 && game->maxPlayers != 1) {
        // winners == true so VICT sent to player when removed
        removePlayers(game, true);
        return;
    } else {
        // winners == false so ELIM sent to player when removed
        removePlayers(game, false);
    }
}

/**
 Attempts to build a game from incoming connections. The game will be torn down if the minimum player count is not
 reached. A maximum time frame of TIMEOUT is given to accept incoming players.

 @note  The game operates in two separate game modes, as specified by command line options in main. If there is only
        one player, the game will begin a single player game after they connect. If more players are specified a
        minimum of four players are required for the game to start (it will tear down with less then four).

 @param lives   The number of lives each player starts with.
 */
void setupGame(GAME* game, int lives)
{

    game->playerCount = 0;
    game->playerHead = NULL;
    game->playerTail = NULL;

    int timeout = TIMEOUT;
    time_t startTime = time(NULL);
    struct pollfd* socketfds = game->readSet;

    // Wait for players to connect for 30s or until max player count is reached
    while (timeout > 0 && game->playerCount < game->maxPlayers) {
        // Program blocks here:
        int rc = poll(socketfds, MAX_EVER_PLAYERS, timeout);
        if (rc == 0) {
            fprintf(stderr, "Poll timed out.\n");
            break;
        }
        if (rc == -1) {
            perror("Poll failed");
            break;
        }
        if (socketfds[0].revents == POLLIN) {
            if (acceptConnection(game, lives))
                printf("Player constructed\n");
            else
                printf("Error constructing player\n");
        }
        timeout = TIMEOUT - ((time(NULL) - startTime) * 1000);
    }

    if (game->playerCount < MIN_PLAYERS && game->maxPlayers != 1) {
        printf("Not enough players connected pid: %d\n", getpid());
        sendMsgToAllPlayers(game, "CANCEL");
        teardownGame(game);
    }
    return;
}
