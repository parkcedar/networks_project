/**
  @file
  @brief    The `main' file that runs the game.

  server.c
  Networks-Project

  This file handles the command line options, sets up the server to listen on a socket and then controls the main game
  loop.

  Adapted from code found at https://github.com/macintosh/echo-servers.c (Copyright (c) 2014 Mathias Buus)
  Modified for this project by: Copyright 2019 Nicholas Pritchard, Ryan Bunney

  Copyright © 2019 All rights reserved.
  @author Lachlan Russell        22414249
  @author Benjamin Longbottom    22234771
  */

#include "connections.h"
#include "game.h"
#include "main.h"

/**
 Enters the main game loop.
 @warning No return.
 */
void gameLoop(GAME* thisGame)
{

    for (int i = 0; thisGame->playerCount > 0; i++) {
        printf("\n\nRound: %d\n", i);
        waitForMoves(thisGame);
        playGameRound(thisGame);
        notifyPlayers(thisGame);
    }

    printf("Program has successfully ended\n");
    teardownGame(thisGame);
    exit(EXIT_SUCCESS);
}

/**
 Prints the program usage.
 @warning No return.
 */
void usage(char* firstArg)
{
    fprintf(stderr, "\nUsage: \t%s [port]\n"
                    "\t%s [-m max players] [-l lives] [port]\n"
                    "If -m is not specified, the game will default to single player mode.\n"
                    "If -l is not specified, the game will default to 3 lives.\n"
                    "\n"
                    "Note max players must be between 1 and 49.\n"
                    "Lives must be between 1 and 99 (inclusive).\n\n",
        firstArg, firstArg);
    exit(EXIT_FAILURE);
}

int main(int argc, char* argv[])
{
    GAME thisGame;
    thisGame.maxPlayers = 10; // Current default.
    thisGame.serverFd = socket(AF_INET, SOCK_STREAM, 0);
    int lives = 3;

    int opt;
    while ((opt = getopt(argc, argv, "m:l:")) != -1) {
        switch (opt) {
        case 'm':
            thisGame.maxPlayers = atoi(optarg);
            if (thisGame.maxPlayers > MAX_EVER_PLAYERS - 1 || thisGame.maxPlayers < 1) {
                fprintf(stderr, "This game is restricted to a maximum of %i players.\n", MAX_EVER_PLAYERS - 1);
                usage(argv[0]);
            } else if (thisGame.maxPlayers < MIN_PLAYERS && thisGame.maxPlayers != 1) {
                fprintf(stderr, "Multiplayer mode requires at least %i players.\n", MIN_PLAYERS);
                usage(argv[0]);
            }
            break;
        case 'l':
            lives = atoi(optarg);
            if (lives > 99 || lives < 1) {
                fprintf(stderr, "This game requires lives set between 1 and 99 inclusive\n");
                usage(argv[0]);
            }
            break;
        case ':':
            fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            usage(argv[0]);
        case '?':
            if (isprint(optopt))
                fprintf(stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf(stderr, "Unknown option character.\n");
            usage(argv[0]);
        default:
            fprintf(stderr, "Unknown error.\n");
            usage(argv[0]);
        }
    }
    // Usage
    if (argc < 2) {
        usage(argv[0]);
    }

    if (thisGame.serverFd < 0) {
        perror("Could not create socket");
        exit(EXIT_FAILURE);
    }

    int opt_val = 1;
    int port = atoi(argv[argc - 1]); // The last argument.
    struct sockaddr_in server;

    // AF_INET -> IPv4
    // INARRY_ANY -> IPv4 local host address
    // htons() & htonl() -> converts ports & addresses into system network values.
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr.s_addr = htonl(INADDR_ANY);

    // SOL_SOCKET = sockets api level
    // SO_REUSEADDR = which option we are changing
    // opt_val = enable SO_REUSEADDR
    if (setsockopt(thisGame.serverFd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof opt_val) < 0) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }

    if (bind(thisGame.serverFd, (struct sockaddr*)&server, sizeof(server)) < 0) {
        perror("Could not bind socket");
        exit(EXIT_FAILURE);
    }

    // Assign abover server struct to the game.
    thisGame.serverInfo = &server;

    // Listen on the port. 128 is the backlog queue length of pending connections.
    if (listen(thisGame.serverFd, 128) < 0) {
        perror("Could not listen on socket");
        exit(EXIT_FAILURE);
    }

    // Clear file descriptors.
    memset(thisGame.readSet, 0, sizeof(thisGame.readSet));
    thisGame.readSet[0].fd = thisGame.serverFd;
    thisGame.readSet[0].events = POLLIN;

    printf("Server is listening on %d\n", port);

    { // -- Initiate game -- //
        srand(time(NULL)); // seed the dice

        setupGame(&thisGame, lives);
        usleep(0.5 * 1000 * 1000); // Allows a buffer for underperforming clients.
        printf("Starting game pid: %d\n", getpid());

        // send start messages to all players.
        char msg[MESSAGE_SIZE];
        snprintf(msg, MESSAGE_SIZE, "START,%02d,%02d", thisGame.playerCount, lives);

        sendMsgToAllPlayers(&thisGame, msg);
        gameLoop(&thisGame);
    }
}
