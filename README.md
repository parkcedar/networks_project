# Networks_Project
# README

## SYSTEM: Mac Labs

## Build Project

This project has been supplied with a makefile.
Navigate to the `src/` directory and run the makefile from there. 
The executable will be in the `src/` directory once make has finished.

## Run Project

After running `make` the executable will be named `server`

Usage:    `./server [port]`
               `./server [-m max players] [-l lives] [port]`
If `-m [Max players]` is not specified, the game will default to single player mode. If this option is specified a minimum of 4 players are required to connect or the server will tear itself down gracefully.
If `-l [Lives]` is not specified, the game will default to `3` lives.

Note max players must be between `1` and `49`.
Lives must be between `1` and `99` (inclusive).

## Behaviour 
The server will run a single game, then tear down all connections and exit.
A game ends when either all players are eliminated or disconnected.  If all but one player is eliminated, the last player will continue to play rounds until it runs out of lives.
In single player mode, the player always loses (sent an `elim` packet).

## Incoming connections

Due to the apple filewall running on the mac labs, shortly after running the program a dialoge will pop up asking to accept incoming network connections. If the server is running on localhost port, 4444 then this notice can be denied and the program will continue execution as expected.

In our testing we were unable to accept this notice due to admin priveleges on the mac labs. We did discover a circumvention however.

*Note: This is not required to test our program, just included for your interest.*
Included in the project is a precompiled executable named `signedServer`. This is the same executable as the one you created from above, however it has been signed with an Apple Developer certificate. Running this version will not cause the dialoge box to pop up.
