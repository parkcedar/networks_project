var game_8c =
[
    [ "constructPlayer", "game_8c.html#a5db583220af993ab4a544e0f56ca9137", null ],
    [ "notifyPlayers", "game_8c.html#a16f4b8e6418b7c43fa39a0815b54d780", null ],
    [ "playerDisconnected", "game_8c.html#ac400b8be4e6d9465388075783044d0a5", null ],
    [ "playGameRound", "game_8c.html#a020e0cd2d8dac343a9f793e75d69af8f", null ],
    [ "readMoves", "game_8c.html#ac4b3524ed84d8c3beeecc635f323a3d2", null ],
    [ "removePlayer", "game_8c.html#af5e4e692bac80c7a54b2652c849d9f45", null ],
    [ "removePlayers", "game_8c.html#a2c203a4de4ee0b191ca86e3a710ea0b6", null ],
    [ "setupGame", "game_8c.html#a94e29b622818decf7437d46902164e82", null ]
];