var struct__game =
[
    [ "maxPlayers", "struct__game.html#aa1b24fe78bd4f1d94890fc632e97151f", null ],
    [ "playerCount", "struct__game.html#a970e4524261782959a63be471548f3ba", null ],
    [ "playerHead", "struct__game.html#ac527d5bf6427ca98ffdee557ad838b3d", null ],
    [ "playerTail", "struct__game.html#a5340bdf22c5d408aa82473ab8f369038", null ],
    [ "readSet", "struct__game.html#a798af02f013a05594222f4312a783a31", null ],
    [ "serverFd", "struct__game.html#aa6b131f528e28e2f9b3ea20ecc8933eb", null ],
    [ "serverInfo", "struct__game.html#a7c409dc34e38ef6136eff144c954e31a", null ]
];