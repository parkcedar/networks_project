var game_8h =
[
    [ "constructPlayer", "game_8h.html#a5db583220af993ab4a544e0f56ca9137", null ],
    [ "notifyPlayers", "game_8h.html#a16f4b8e6418b7c43fa39a0815b54d780", null ],
    [ "playerDisconnected", "game_8h.html#ac400b8be4e6d9465388075783044d0a5", null ],
    [ "playGameRound", "game_8h.html#a020e0cd2d8dac343a9f793e75d69af8f", null ],
    [ "readMoves", "game_8h.html#ac4b3524ed84d8c3beeecc635f323a3d2", null ],
    [ "removePlayer", "game_8h.html#af5e4e692bac80c7a54b2652c849d9f45", null ],
    [ "setupGame", "game_8h.html#a94e29b622818decf7437d46902164e82", null ]
];