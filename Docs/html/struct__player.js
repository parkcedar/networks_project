var struct__player =
[
    [ "alive", "struct__player.html#a23a0742ddb120a88b03c4eaa61daab72", null ],
    [ "clientInfo", "struct__player.html#a46ce11e41b2188d6798efbf80cd510d6", null ],
    [ "fd", "struct__player.html#a6f8059414f0228f0256115e024eeed4b", null ],
    [ "id", "struct__player.html#a7441ef0865bcb3db9b8064dd7375c1ea", null ],
    [ "lastMessage", "struct__player.html#a5378714972b46b7655841f52e7e420b5", null ],
    [ "lives", "struct__player.html#ac1d68ef9a5dd304c5015e07af7f2bb19", null ],
    [ "next", "struct__player.html#a41638de2d34cf67164ebf776122a8706", null ],
    [ "passedLastRound", "struct__player.html#ab470f766e02f6a1c4a2a4c49312ea71c", null ],
    [ "previous", "struct__player.html#a1d2349409d67f0695396acd8adcd8ea9", null ],
    [ "read", "struct__player.html#af49a80be54293d8b153cd2a3dfd4e068", null ]
];