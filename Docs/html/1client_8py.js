var 1client_8py =
[
    [ "Game", "class1client_1_1_game.html", "class1client_1_1_game" ],
    [ "cancel", "1client_8py.html#a5ffca7b9532d986b9a224a359837ffb7", null ],
    [ "con_move", "1client_8py.html#a675ed6a0f63c76497cf48011cf5eb51f", null ],
    [ "elim", "1client_8py.html#a755bee5a1baa35d4d91e5a1865115e51", null ],
    [ "fail", "1client_8py.html#a56377a6b123cbba242bd7a8aa952d78b", null ],
    [ "init", "1client_8py.html#a9c61d80f343d7386a9c436b1fbfe7a47", null ],
    [ "next_round", "1client_8py.html#af85cc6fc8cee01f2b0b80c653534ac53", null ],
    [ "parse_message", "1client_8py.html#a29a0c07ecc93e25673792c154d003e10", null ],
    [ "pass_func", "1client_8py.html#aa25fc5fe7f45cf5d78a47761dd818138", null ],
    [ "reject", "1client_8py.html#ab23ad70a7cf4652b1271d0042ee34ff5", null ],
    [ "sendMove", "1client_8py.html#a45e69c7c0226b5e032639bfe504f3193", null ],
    [ "start", "1client_8py.html#adcf7b6e217638b45e54fa10f4473fe9b", null ],
    [ "vict", "1client_8py.html#a1516f7d10020132c7de5b216df5653a2", null ],
    [ "message", "1client_8py.html#ae1ed0d7a6f352c7ee3ad978429822c6f", null ],
    [ "recieved", "1client_8py.html#a094ffa6d6dd680fafdae9ba1695d5a2a", null ],
    [ "server_address", "1client_8py.html#a47b1e5a3992c5c4230058925d22dc1e0", null ],
    [ "sock", "1client_8py.html#a6f3a42a6e2447bdc41d1dedba98c762c", null ],
    [ "thisGame", "1client_8py.html#a33620cd5ac1a6230edcf299f53b9f8d3", null ]
];