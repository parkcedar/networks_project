var connections_8h =
[
    [ "acceptConnection", "connections_8h.html#ae2e814e92818319c423bda9e1560253f", null ],
    [ "parseMessage", "connections_8h.html#acfa3d04658d6df7a1885def968ffd4be", null ],
    [ "readMessage", "connections_8h.html#a8fc4b120726f1074f275fec48589a50c", null ],
    [ "rejectConnection", "connections_8h.html#a44002538b1df0e2b21f1c55ddf18d8c7", null ],
    [ "sendMessage", "connections_8h.html#a2f87591eb9b331e6846e607f36230ffe", null ],
    [ "sendMsgToAllPlayers", "connections_8h.html#a6fd8b0fe8e0165152e5d928ad3d5602e", null ],
    [ "teardownGame", "connections_8h.html#ad26fa0c0e0f5ec1dea36f3f14cd1559b", null ],
    [ "waitForMoves", "connections_8h.html#a082e05b95ca2f58351c3dc8d0359af01", null ]
];