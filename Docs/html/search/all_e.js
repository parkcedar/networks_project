var searchData=
[
  ['read',['read',['../struct__player.html#af49a80be54293d8b153cd2a3dfd4e068',1,'_player']]],
  ['readmessage',['readMessage',['../connections_8c.html#a8fc4b120726f1074f275fec48589a50c',1,'readMessage(int client_fd):&#160;connections.c'],['../connections_8h.html#a8fc4b120726f1074f275fec48589a50c',1,'readMessage(int client_fd):&#160;connections.c']]],
  ['readmoves',['readMoves',['../game_8c.html#ac4b3524ed84d8c3beeecc635f323a3d2',1,'readMoves(GAME *game):&#160;game.c'],['../game_8h.html#ac4b3524ed84d8c3beeecc635f323a3d2',1,'readMoves(GAME *game):&#160;game.c']]],
  ['readset',['readSet',['../struct__game.html#a798af02f013a05594222f4312a783a31',1,'_game']]],
  ['recieved',['recieved',['../namespaceclient.html#a094ffa6d6dd680fafdae9ba1695d5a2a',1,'client']]],
  ['rejectconnection',['rejectConnection',['../connections_8c.html#a44002538b1df0e2b21f1c55ddf18d8c7',1,'rejectConnection(int serverFd):&#160;connections.c'],['../connections_8h.html#a44002538b1df0e2b21f1c55ddf18d8c7',1,'rejectConnection(int serverFd):&#160;connections.c']]],
  ['removeplayer',['removePlayer',['../game_8c.html#af5e4e692bac80c7a54b2652c849d9f45',1,'removePlayer(GAME *game, PLAYER *player):&#160;game.c'],['../game_8h.html#af5e4e692bac80c7a54b2652c849d9f45',1,'removePlayer(GAME *game, PLAYER *player):&#160;game.c']]],
  ['removeplayers',['removePlayers',['../game_8c.html#a2c203a4de4ee0b191ca86e3a710ea0b6',1,'game.c']]],
  ['round',['round',['../classclient_1_1_game.html#a08fcf5be300f50771cdd6d21174b4610',1,'client::Game']]]
];
