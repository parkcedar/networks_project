var searchData=
[
  ['main',['main',['../server_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'server.c']]],
  ['main_2eh',['main.h',['../main_8h.html',1,'']]],
  ['max_5fever_5fplayers',['MAX_EVER_PLAYERS',['../main_8h.html#a3c87850a84d8e93c9fb9147bb0daffe9',1,'main.h']]],
  ['maxplayers',['maxPlayers',['../struct__game.html#aa1b24fe78bd4f1d94890fc632e97151f',1,'_game']]],
  ['message',['message',['../namespaceclient.html#ae1ed0d7a6f352c7ee3ad978429822c6f',1,'client']]],
  ['message_5fsize',['MESSAGE_SIZE',['../main_8h.html#aeca90e1c1c62b70670514ffc18c9dfd4',1,'main.h']]],
  ['messagetype',['messageType',['../struct__client_message.html#a90daa62357774bd27a186ed95408661b',1,'_clientMessage']]],
  ['mili',['MILI',['../main_8h.html#acc322ee6d355738281965a92787723b7',1,'main.h']]],
  ['min_5fplayers',['MIN_PLAYERS',['../main_8h.html#ab1de365cca5246cc09e4bd07dbad5c29',1,'main.h']]],
  ['mov',['MOV',['../main_8h.html#a2d5e8800f4f22aa29aeaee51ebb27df4aa1535ce8fd6caf08009dcae925741d9b',1,'main.h']]],
  ['move',['move',['../struct__client_message.html#a707bc85106dc2561287fe25535a9494a',1,'_clientMessage']]],
  ['movetype',['moveType',['../main_8h.html#a1021a664073e541a297e7ee35597e5fc',1,'main.h']]],
  ['my_5flives',['my_lives',['../classclient_1_1_game.html#a5fbcf5e2b0766d4b9688d7e83af20a6c',1,'client::Game']]]
];
