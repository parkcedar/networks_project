var searchData=
[
  ['sendmessage',['sendMessage',['../connections_8c.html#a2f87591eb9b331e6846e607f36230ffe',1,'sendMessage(int client_fd, char *message):&#160;connections.c'],['../connections_8h.html#a2f87591eb9b331e6846e607f36230ffe',1,'sendMessage(int client_fd, char *message):&#160;connections.c']]],
  ['sendmove',['sendMove',['../namespaceclient.html#a0400a3d07db54dabb3a64df6dd816e1b',1,'client']]],
  ['sendmsgtoallplayers',['sendMsgToAllPlayers',['../connections_8c.html#a6fd8b0fe8e0165152e5d928ad3d5602e',1,'sendMsgToAllPlayers(GAME *game, char *msg):&#160;connections.c'],['../connections_8h.html#a6fd8b0fe8e0165152e5d928ad3d5602e',1,'sendMsgToAllPlayers(GAME *game, char *msg):&#160;connections.c']]],
  ['setupgame',['setupGame',['../game_8c.html#a94e29b622818decf7437d46902164e82',1,'setupGame(GAME *game, int lives):&#160;game.c'],['../game_8h.html#a94e29b622818decf7437d46902164e82',1,'setupGame(GAME *game, int lives):&#160;game.c']]],
  ['start',['start',['../namespaceclient.html#a28675c6e1e45a3f1402fc95dd1158891',1,'client']]]
];
