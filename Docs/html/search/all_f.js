var searchData=
[
  ['sendmessage',['sendMessage',['../connections_8c.html#a2f87591eb9b331e6846e607f36230ffe',1,'sendMessage(int client_fd, char *message):&#160;connections.c'],['../connections_8h.html#a2f87591eb9b331e6846e607f36230ffe',1,'sendMessage(int client_fd, char *message):&#160;connections.c']]],
  ['sendmove',['sendMove',['../namespaceclient.html#a0400a3d07db54dabb3a64df6dd816e1b',1,'client']]],
  ['sendmsgtoallplayers',['sendMsgToAllPlayers',['../connections_8c.html#a6fd8b0fe8e0165152e5d928ad3d5602e',1,'sendMsgToAllPlayers(GAME *game, char *msg):&#160;connections.c'],['../connections_8h.html#a6fd8b0fe8e0165152e5d928ad3d5602e',1,'sendMsgToAllPlayers(GAME *game, char *msg):&#160;connections.c']]],
  ['server_2ec',['server.c',['../server_8c.html',1,'']]],
  ['server_5faddress',['server_address',['../namespaceclient.html#a47b1e5a3992c5c4230058925d22dc1e0',1,'client']]],
  ['serverfd',['serverFd',['../struct__game.html#aa6b131f528e28e2f9b3ea20ecc8933eb',1,'_game']]],
  ['serverinfo',['serverInfo',['../struct__game.html#a7c409dc34e38ef6136eff144c954e31a',1,'_game']]],
  ['setupgame',['setupGame',['../game_8c.html#a94e29b622818decf7437d46902164e82',1,'setupGame(GAME *game, int lives):&#160;game.c'],['../game_8h.html#a94e29b622818decf7437d46902164e82',1,'setupGame(GAME *game, int lives):&#160;game.c']]],
  ['sock',['sock',['../namespaceclient.html#a6f3a42a6e2447bdc41d1dedba98c762c',1,'client']]],
  ['start',['start',['../namespaceclient.html#a28675c6e1e45a3f1402fc95dd1158891',1,'client']]]
];
