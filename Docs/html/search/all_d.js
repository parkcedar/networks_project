var searchData=
[
  ['parse_5fmessage',['parse_message',['../namespaceclient.html#a7f16c42cd5da970e6f7bfa927fef0796',1,'client']]],
  ['parsemessage',['parseMessage',['../connections_8c.html#acfa3d04658d6df7a1885def968ffd4be',1,'parseMessage(CLIENT_MESSAGE *container, char *message):&#160;connections.c'],['../connections_8h.html#acfa3d04658d6df7a1885def968ffd4be',1,'parseMessage(CLIENT_MESSAGE *container, char *message):&#160;connections.c']]],
  ['pass_5ffunc',['pass_func',['../namespaceclient.html#a5b240ed862f960c17deba7bca6afdb8b',1,'client']]],
  ['passedlastround',['passedLastRound',['../struct__player.html#ab470f766e02f6a1c4a2a4c49312ea71c',1,'_player']]],
  ['player',['PLAYER',['../main_8h.html#ae46575717194a2d8858f92b4d7f2b297',1,'main.h']]],
  ['player_5fid',['player_id',['../classclient_1_1_game.html#aa9851c140b46b5f6381701d194f1f4e3',1,'client::Game']]],
  ['playercount',['playerCount',['../struct__game.html#a970e4524261782959a63be471548f3ba',1,'_game']]],
  ['playerdisconnected',['playerDisconnected',['../game_8c.html#ac400b8be4e6d9465388075783044d0a5',1,'playerDisconnected(GAME *game, PLAYER *player):&#160;game.c'],['../game_8h.html#ac400b8be4e6d9465388075783044d0a5',1,'playerDisconnected(GAME *game, PLAYER *player):&#160;game.c']]],
  ['playerhead',['playerHead',['../struct__game.html#ac527d5bf6427ca98ffdee557ad838b3d',1,'_game']]],
  ['playertail',['playerTail',['../struct__game.html#a5340bdf22c5d408aa82473ab8f369038',1,'_game']]],
  ['playgameround',['playGameRound',['../game_8c.html#a020e0cd2d8dac343a9f793e75d69af8f',1,'playGameRound(GAME *game):&#160;game.c'],['../game_8h.html#a020e0cd2d8dac343a9f793e75d69af8f',1,'playGameRound(GAME *game):&#160;game.c']]],
  ['previous',['previous',['../struct__player.html#a1d2349409d67f0695396acd8adcd8ea9',1,'_player']]]
];
