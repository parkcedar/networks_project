var main_8h =
[
    [ "_clientMessage", "struct__client_message.html", "struct__client_message" ],
    [ "_player", "struct__player.html", "struct__player" ],
    [ "_game", "struct__game.html", "struct__game" ],
    [ "BUFFER_SIZE", "main_8h.html#a6b20d41d6252e9871430c242cb1a56e7", null ],
    [ "CATCH", "main_8h.html#a6af153487f125d9f3f325d46e050b350", null ],
    [ "DEPRECATED", "main_8h.html#ac1e8a42306d8e67cb94ca31c3956ee78", null ],
    [ "eMEMORY", "main_8h.html#a97844b8259b9d4ffefd498a57ea644fb", null ],
    [ "ePLAYER", "main_8h.html#af19c902a3bfb4cd607b7aa0a3d913a7e", null ],
    [ "eSOCKET", "main_8h.html#a8ba2adf91afc5cd4926ab3c95172c1cb", null ],
    [ "eSTRING", "main_8h.html#a24b4ee4a0fe7e46fddeb9a998ceac3ea", null ],
    [ "MAX_EVER_PLAYERS", "main_8h.html#a3c87850a84d8e93c9fb9147bb0daffe9", null ],
    [ "MESSAGE_SIZE", "main_8h.html#aeca90e1c1c62b70670514ffc18c9dfd4", null ],
    [ "MILI", "main_8h.html#acc322ee6d355738281965a92787723b7", null ],
    [ "MIN_PLAYERS", "main_8h.html#ab1de365cca5246cc09e4bd07dbad5c29", null ],
    [ "TIMEOUT", "main_8h.html#a45ba202b05caf39795aeca91b0ae547e", null ],
    [ "CLIENT_MESSAGE", "main_8h.html#a7715cca384bc2eeada655cedcccced19", null ],
    [ "GAME", "main_8h.html#aac03bd720afcc02d400142be1eb95011", null ],
    [ "PLAYER", "main_8h.html#ae46575717194a2d8858f92b4d7f2b297", null ],
    [ "clientMessageType", "main_8h.html#a2d5e8800f4f22aa29aeaee51ebb27df4", [
      [ "INVALID", "main_8h.html#a2d5e8800f4f22aa29aeaee51ebb27df4aef2863a469df3ea6871d640e3669a2f2", null ],
      [ "INIT", "main_8h.html#a2d5e8800f4f22aa29aeaee51ebb27df4a0cb1b2c6a7db1f1084886c98909a3f36", null ],
      [ "MOV", "main_8h.html#a2d5e8800f4f22aa29aeaee51ebb27df4aa1535ce8fd6caf08009dcae925741d9b", null ]
    ] ],
    [ "moveType", "main_8h.html#a1021a664073e541a297e7ee35597e5fc", [
      [ "EVEN", "main_8h.html#a1021a664073e541a297e7ee35597e5fca8487756fbc720579906f0ae1738f0fcc", null ],
      [ "ODD", "main_8h.html#a1021a664073e541a297e7ee35597e5fcaa29cedab858353a26006af9db7cd1ed8", null ],
      [ "DOUB", "main_8h.html#a1021a664073e541a297e7ee35597e5fca93b26b56f37e63a6bf47e735247e0301", null ],
      [ "CON", "main_8h.html#a1021a664073e541a297e7ee35597e5fcaa5c511df5582eb12c3723cbf5ade7947", null ]
    ] ]
];