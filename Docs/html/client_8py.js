var client_8py =
[
    [ "Game", "classclient_1_1_game.html", "classclient_1_1_game" ],
    [ "con_move", "client_8py.html#ac68c4abe1723ca275d124ffe9c0d7a04", null ],
    [ "elim", "client_8py.html#acf3e72eab1f386938b5566f3a1cce583", null ],
    [ "fail", "client_8py.html#a3b160d4415fa22ba42bb1006619b7d8f", null ],
    [ "gracefulExit", "client_8py.html#ace0d9832e4adaa87ee4047914e1b29e9", null ],
    [ "init", "client_8py.html#a5f5dd9de5bb9468949a3e548b0f8aac7", null ],
    [ "next_round", "client_8py.html#afbcc345c7df64b0468fb9250f505eff8", null ],
    [ "parse_message", "client_8py.html#a7f16c42cd5da970e6f7bfa927fef0796", null ],
    [ "pass_func", "client_8py.html#a5b240ed862f960c17deba7bca6afdb8b", null ],
    [ "sendMove", "client_8py.html#a0400a3d07db54dabb3a64df6dd816e1b", null ],
    [ "start", "client_8py.html#a28675c6e1e45a3f1402fc95dd1158891", null ],
    [ "vict", "client_8py.html#a1a7d2807ddadd5c30118470218565c25", null ],
    [ "message", "client_8py.html#ae1ed0d7a6f352c7ee3ad978429822c6f", null ],
    [ "recieved", "client_8py.html#a094ffa6d6dd680fafdae9ba1695d5a2a", null ],
    [ "server_address", "client_8py.html#a47b1e5a3992c5c4230058925d22dc1e0", null ],
    [ "sock", "client_8py.html#a6f3a42a6e2447bdc41d1dedba98c762c", null ],
    [ "thisGame", "client_8py.html#a33620cd5ac1a6230edcf299f53b9f8d3", null ]
];